module.exports = {
    apps : [
        {
          name: "Tensa",
          script: "./start.js",
          watch: false,
          env: {
            "NODE_ENV": "production",
          }
        }
    ]
}