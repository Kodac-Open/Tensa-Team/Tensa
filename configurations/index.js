module.exports = {
    ...require("./config"),

    exampleConfig: require("./ex_config"),

    blockedUsers: require("./blockedUsers.json").blockedUsers
}