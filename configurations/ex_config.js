module.exports = {

    // Token for the bot.
    botToken: "",

    // Owner/s of the bot.
    botMaintainers: [ "" ],

    // Prefix for the bot.
    botPrefix: "!",

    // Token of the webhook.
    webhookToken: "",

    // ID of the webhook.
    webhookID: "",

    // Port for the http broadcast.
    httpPort: 8080,

    // Format to have the log files in, check https://www.npmjs.com/package/time-stamp for more info.
    logDateFormat: "DD-MM-YYYY",

    /** 
     * The amount of shards you want.
     * cpu - One shard per CPU core.
     * auto - Let Discord automatically descide the amount of shards.
     * Number (e.g. 4) - Defne an amount of shards yourself.
     * none - No sharding.
     */ 
    shardAmount: "cpu",

    /**
     * The url for the database to connect to.
     * 
     * Format: 
     *      mongodb://0.0.0.0:27017/collection
     * OR
     *      mongodb://username:password@0.0.0.0:27017/collection
     */
    dbUrl: "mongodb://127.0.0.1:27017/tensa"

}
