/**
 * @file Starting point for a shard.
 * @author oskikiboy
 */

// Globals.
global.config = require("../configurations/");
global.logger = require("./Internals/logger");

// Setting up.
const { Util: { addToGlobal } } = require("./Internals/");
const { version } = require("../package.json");
const driver = require("./database/");
const log = new logger("Launcher");

// Object defines.
Object.defineProperty(Array.prototype, "chunk", {
	value: function value (n) {
		return Array.from(Array(Math.ceil(this.length / n)), (_, i) => this.slice(i * n, (i * n) + n));
	},
});

// Object assignments.
Object.assign(String.prototype, {
	escapeRegex () {
		const matchOperators = /[|\\{}()[\]^$+*?.]/g;
		return this.replace(matchOperators, "\\$&");
	},
});

log.info(`Starting Tensa, Version ${version}.`);

/**
 * This initialises the database and the bot.
 * 
 * @async
 * @function
 */
(async () => {
	try {
		const database = new driver(config.dbUrl, log);
		await database.connect();
		addToGlobal("db", database);
		if (config.shardAmount === "none") {
			require("./modules/longtaskWorker");
		}
		require("./bot/");
	} catch (err) {
		throw err;
	}
})().catch(err => {
	log.crit(`An error occoured during boot.\n ${err.stack}`);
});

process.on("unhandledRejection", reason => {
	log.error(`An unexpected error occurred.\n`, reason);
});

process.on("uncaughtException", err => {
	log.error(`An unexpected and unknown error occurred.\n`, err);
	process.exit(1);
});
