module.exports = Schema => new Schema({
	id: { type: String },
	level: { type: Number, default: 0 },
});
