module.exports = Schema => new Schema({
	locale: { type: String, default: "en" },
	prefix: { type: String, default: config.botPrefix },
	blocked: { type: Boolean, default: false },
});
