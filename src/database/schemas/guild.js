module.exports = Schema => new Schema({
	id: { type: String },
	settings: { type: require("./guildSettings")(Schema), required: true },
	perms: { type: Object, default: [{}] },
	support: { type: Boolean, default: false },
});
