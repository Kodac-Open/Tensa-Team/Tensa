module.exports = Schema => new Schema({
	id: { type: String },
	settings: { type: require("./settings")(Schema), required: true },
	maintainer: { type: Boolean, default: false },
});
