const cachegoose = require("cachegoose");

class Driver {
	constructor (opts, log) {
		this.mongoose = require("mongoose");

		this.db = null;

		this.dbUrl = opts;

		this.schema = this.mongoose.Schema;

		this.log = log;
	}

	connect () {
		return new Promise((resolve, reject) => {
			cachegoose(this.mongoose);

			this.mongoose.connect(this.dbUrl, {
				promiseLibrary: global.Promise,
				connectTimeoutMS: 30000,
				socketTimeoutMS: 30000,
				autoReconnect: true,
				keepAlive: 120,
				poolSize: 100,
			});

			this.db = this.mongoose.connection;

			this.mongoose.Promise = global.Promise;

			this.loadSchemas();

			this.db.on("err", err => {
				reject(err);
			});

			this.db.once("open", () => {
				this.log.suc("Successfuly connected to MongoDB");

				resolve(this);
			});
		});
	}

	loadSchemas () {
		let guild = require("./schemas/guild")(this.schema);
		this.mongoose.model("guilds", guild);
	}

	/**
	 * This method returns a new guild or finds a previous one
	 * 
	 * @function fetchOrCreateGuild
	 * @param guild
	 * @returns {Promise}
	 */
	fetchOrCreateGuild (guild) {
		return new Promise((resolve, reject) => {
			this.models.guilds.findOne({ id: String(guild.id) }).then(g => {
				if (g) { resolve({ Guild: g, new: false }); } else {
					this.getNewModelForGuild(guild).then(newGuild => {
						resolve({ Guild: newGuild, new: true });
					}).catch(err => {
						reject(err);
					});
				}
			})
				.catch(err => {
					reject(err);
				});
		});
	}

	/**
	 * Retuns a new model for a guild object
	 * @async
	 * @function getNewModelForGuild
	 * @param guild
	 * @returns {Object} newGuild
	 */
	async getNewModelForGuild (guild) {
		let newGuild = await this.models.guilds.create(new this.models.guilds({
			id: String(guild.id),
			settings: {},
			support: guild.id === "405315401095053312",
		}));
		return newGuild;
	}

	get models () {
		return this.mongoose.models;
	}
}

module.exports = Driver;
