/**
 * Ran when the client disconnects.
 * 
 * @function
 * @param {Object} log
 */
exports.run = ({ client, log }) => {
	log.warn(`Disconnected at ${new Date()}.`);
	client.destroy();
	process.exit(0);
};
