/**
 * Ran when the client reconnects.
 * 
 * @function
 * @param {Object} 
 */
exports.run = ({ client, log }) => {
	log.warn(`Reconnecting at ${new Date()}.`);
	// eslint-disable-next-line
	if (Boolean(process.env.SHARDS_READY)) {
		client.shard.fetchClientValues("users.size").then(results => {
			client.user.setActivity(`${config.botPrefix}help | Serving ${results.reduce((prev, val) => prev + val, 0)} users! | Shard ${process.env.SHARD_ID}`, { type: "PLAYING" });
		}).catch(err => log.error(err));
	} else {
		client.user.setActivity(`${config.botPrefix}help | Serving ${client.users.size} users!`, { type: "PLAYING" });
	}
};
