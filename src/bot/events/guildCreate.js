/* eslint-disable no-inline-comments */

/**
 * Ran when a guild is added.
 * 
 * @function
 * @param {Object} 
 * @param {Object} guild
 */
exports.run = ({ client, log /* Watcher*/ }, guild) => {
	/* Watcher.IPC.send("guild", `Joined new guild! ${guild.name} (${guild.id}).`);*/
	db.fetchOrCreateGuild(guild).then(g => {
		if (g.new) { log.info(`Joined new guild ${guild.name} (${g.Guild.id})... added to database`); } else {
			log.info(`Joined guild! ${guild.name} (${g.Guild.id})`);
		}
	});
	// eslint-disable-next-line
	if (Boolean(process.env.SHARDS_READY)) {
		client.shard.fetchClientValues("users.size").then(results => {
			client.user.setActivity(`${config.botPrefix}help | Serving ${results.reduce((prev, val) => prev + val, 0)} users! | Shard ${process.env.SHARD_ID}`, { type: "PLAYING" });
		}).catch(err => log.error(err));
	} else {
		client.user.setActivity(`${config.botPrefix}help | Serving ${client.users.size} users!`, { type: "PLAYING" });
	}
};
