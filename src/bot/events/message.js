/**
 * Ran when the client recieves a message.
 * 
 * @function
 * @param {Object}
 * @param {Object} message
 */
exports.run = ({ client }, message) => {
	client.runCommand(message);
};
