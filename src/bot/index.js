/**
 * @file The bot itself.
 * @author oskikiboy
 */

// Discord.js Client.
const { Client, log } = require("./lib/Client");
log.silly("Creating Discord.js client.");

// Initialise an instance of the Client
const disabledEvents = ["TYPING_START"];
const client = new Client({
	disabledEvents: disabledEvents,
	messageCacheLifetime: 1800,
	messageSweepInterval: 900,
	messageCacheMaxSize: 1000,
	restTimeOffset: 50,
});

// Log into the Client.
log.info("Logging into Discord with token", config.botToken.replace(/^.{48}/g, "*************"));
client.login(config.botToken).catch(err => {
	log.error(`An error occoured while logging into Discord \n${err}`);
	client.destroy();
});
