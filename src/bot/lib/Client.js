/**
 * Some bits taken from https://github.com/KingDGrizzle/Vlag-Bot
 * 
 * @file The Client for the bot to run.
 * @author oskikiboy
 */

const log = new logger(config.shardAmount !== "none" ? `Discord - Shard ${process.env.SHARD_ID}` : `Discord`);
const { Client: DJSClient, Collection } = require("discord.js");
const reload = require("require-reload")(require);

// eslint-disable-next-line no-inline-comments
const { Util /* Watcher*/ } = require("../../Internals/");
const { readDir, Constants } = Util;
const { blockedUsers, botPrefix } = require(`../../../configurations`);

/**
 * Client Constructor.
 * 
 * @constructor
 * @augments DJSClient - Extends to the Discord.js Client.
 * @param {Object} options - Options to initialise the client.
 */
class Client extends DJSClient {
	constructor (options) {
		super(options);
		this.commands = new Collection();
		this.cEvents = new Collection();
		this.log = log;
		this.Constants = Constants;
		this.cmdExec = 0;
		this.website = new (require("../../website/"))(this);
		this.websiteInit = false;
		// This.Watcher = new Watcher(this);

		this.on("ready", async () => {
			this.log.suc(`Succesfuly logged into Discord as ${this.user.tag}${this.shard ? `. Shard ${this.shard.id}` : ","} with ${this.users.size} users and ${this.guilds.size} guilds!`);
			if (this.shard && Number(process.env.SHARD_ID) + 1 === Number(process.env.SHARD_COUNT)) {
				await this.shard.broadcastEval("process.env.SHARDS_READY = true");
			}

			this.setGame();
			this.init();
		});
	}

	/**
	 * Runs the initial tasks.
	 * 
	 * @async
	 * @function init
	 */
	async init () {
		let cmdFiles = await readDir("./bot/commands/");
		let evFiles = await readDir("./bot/events/");

		this.reloadAllCommands(cmdFiles);
		this.loadAllEvents(evFiles);

		setInterval(() => {
			this.setGame();
		}, 100000);

		if (!this.websiteInit) {
			this.website.init().then(() => {
				this.websiteInit = true;
			});
		}
	}

	/**
	 * Sets the game.
	 * 
	 * @async
	 * @function setGame
	 */
	async setGame () {
		// eslint-disable-next-line
		if (Boolean(process.env.SHARDS_READY)) {
			this.shard.fetchClientValues("users.size").then(results => {
				this.user.setActivity(`${config.botPrefix}help | Serving ${results.reduce((prev, val) => prev + val, 0)} users! | Shard ${this.shard.id}`, { type: "PLAYING" });
			}).catch(err => this.log.error(err));
		} else {
			this.user.setActivity(`${config.botPrefix}help | Serving ${this.users.size} users!`, { type: "PLAYING" });
		}
	}

	/**
	 * Loads all the events using the array.
	 * 
	 * @function loadAllEvents
	 * @param {Array} eventFiles
	 */
	loadAllEvents (eventFiles) {
		for (const event of eventFiles) {
			if (event.startsWith("_")) continue;
			let eventReload = reload(`../events/${event}`);
			// eslint-disable-next-line
			this.on(event.split(".")[0], (...args) => eventReload.run({ client: this, log: this.log /* Watcher: this.Watcher*/ }, ...args));
		}
	}

	/**
	 * Reloads/Loads a command
	 * 
	 * @param {String} command 
	 * @returns {<Promise>}
	 * @throws An error.
	 */
	reloadCommand (command) {
		try {
			let cmdReload = reload(`../commands/${command}.js`);
			let cmd = new cmdReload(this);

			this.commands.set(command, cmd);
			return Promise.resolve();
		} catch (err) {
			return Promise.reject(err);
		}
	}

	/**
	 * Reloads the commands.
	 * 
	 * @function reloadAllCommands
	 * @param {Array} commandFiles 
	 */
	reloadAllCommands (commandFiles, reloading = false) {
		for (const command of commandFiles) {
			if (command.startsWith("_")) continue;
			this.reloadCommand(command.split(".")[0]).catch(err => {
				this.log.error(`Failed to ${reloading ? "reload" : "load"} command ${command}:\n${err.stack}`);
			});
		}
	}

	/**
	 * Gets the number of commands.
	 * 
	 * @returns {Number}
	 */
	get totalCommands () {
		return this.commands.size;
	}

	/**
	 * Creates a command string and a suffix string, and puts it in anobject.
	 * 
	 * @author KingDGrizzle
	 * 
	 * @function checkCommandTag
	 * @param {String} message - Message to be converted to command and suffix
	 * @returns {Object} - Object containing the command and suffix.
	 */
	checkCommandTag (message, guildPrefix) {
		message = message.trim();

		let object = {
			command: null,
			suffix: null,
		};

		let cmdStr;
		if (message.startsWith(guildPrefix)) {
			cmdStr = message.substring(guildPrefix.length);
		}

		if (cmdStr && !cmdStr.includes(" ")) {
			object = {
				command: cmdStr.toLowerCase(),
				suffix: null,
			};
		} else if (cmdStr) {
			let command = cmdStr.split(" ")[0].toLowerCase();
			let suffix = cmdStr.split(" ")
				.splice(1)
				.join(" ")
				.trim();

			object = {
				command: command,
				suffix: suffix,
			};
		}
		return object;
	}

	/**
	 * Sets a guilds prefix.
	 * 
	 * @async
	 * @function runCommand
	 * @param {Object} guild 
	 * @param {String} prefix 
	 */
	async setPrefix (guild, prefix) {
		let { Guild: g } = await db.fetchOrCreateGuild(guild);
		g.settings.prefix = prefix;
		g.save();
	}

	/**
	 * Runs a command
	 * 
	 * @async
	 * @function runCommand
	 * @param {Object} msg 
	 */
	async runCommand (msg) {
		let guildPrefix;

		if (msg.guild) {
			let { Guild } = await db.fetchOrCreateGuild(msg.guild);
			guildPrefix = Guild.settings.prefix;
		} else {
			guildPrefix = botPrefix;
		}

		if (!msg.content.startsWith(guildPrefix) || msg.author.bot) return;

		if (blockedUsers.includes(msg.author.id)) {
			msg.react("❌");

			let m = await msg.channel.send({
				embed: {
					color: this.Constants.embedColours.ERROR,
					description: `Uh-oh it seems that you were blocked from using the bot!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
					},
				},
			});

			setTimeout(() => {
				m.delete();
			}, 2000);
			return;
		}

		let msgObject = this.checkCommandTag(msg.content, guildPrefix);
		let { command, suffix } = msgObject;

		if (!command) return;

		command = this.commands.get(command) || this.commands.find(cmd => cmd.Data.aliases && cmd.Data.aliases.includes(command));

		if (!command) {
			let m = await msg.channel.send({
				embed: {
					color: this.Constants.embedColours.WARN,
					description: `This command **does not exist**!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",
					},
				},
			});

			setTimeout(() => {
				m.delete();
			}, 4000);
			return;
		}

		try {
			this.cmdExec++;
			let { maintainerOnly } = command.Data;

			if (maintainerOnly && this.checkforMaintainer(msg.author.id)) {
				command.notMaintainer({ msg: msg });
			} else {
				command._run({ msg: msg, suffix: suffix, guildPrefix: guildPrefix });
			}
		} catch (error) {
			this.log.error(error);
			let ma = await msg.channel.send({
				embed: {
					color: this.Constants.embedColours.ERROR,
					description: `Oh no, an unknown error has occoured!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
					},
				},
			});

			setTimeout(() => {
				ma.delete();
			}, 4000);
		}
	}

	/**
	 * Checks if a user is a maintainer.
	 * 
	 * @function chechforMaintainer
	 * @param {String|Number} id 
	 * @returns {Boolean}
	 */
	checkforMaintainer (id) {
		if (config.botMaintainers.indexOf(id) === -1) return true;
		else return false;
	}
}

module.exports = { Client: Client, log: log };
