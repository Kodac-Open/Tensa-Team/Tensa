/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const { version } = require("discord.js");
const os = require("os");

/**
 * About command constructor.
 * 
 * @description Gives you information about the bot.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class About extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["ab"],
			usage: prefix => `${prefix}about`,
			info: "Gives you information about the bot.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg }) {
		msg.channel.send({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: `**${this.client.user.username}**, version ${this.tVer}, is a powerful Discord utility bot. Written by **${this.client.users.get("193972602392281088").tag}**${this.newLine}`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
				},
				fields: [
					{
						name: "Bot info:",
						value: `${this.client.user.tag} (${this.client.user.id}).${this.newLine}`,
					},
					{
						name: "Commands Executed:",
						value: `${this.client.cmdExec}.${this.newLine}`,
					},
					{
						name: "Github:",
						value: `You can find the github, [here](${this.gitUrl}).${this.newLine}`,
					},
					{
						name: "Library:",
						value: `Discord.JS, Version ${version}.${this.newLine}`,
					},
					{
						name: "System info:",
						value: `${process.platform}-${process.arch} with ${process.release.name} version ${process.version.slice(1)}.${this.newLine}`,
					},
					{
						name: "Process info (PID):",
						value: `${process.pid}.${this.newLine}`,
					},
					{
						name: "Process memory usage:",
						value: `${Math.ceil(process.memoryUsage().heapTotal / 1000000)} MB.${this.newLine}`,
					},
					{
						name: "System memory usage:",
						value: `${Math.ceil((os.totalmem() - os.freemem()) / 1000000)} of ${Math.ceil(os.totalmem() / 1000000)} MB.`,
					},
				],
			},
		});
	}
};
