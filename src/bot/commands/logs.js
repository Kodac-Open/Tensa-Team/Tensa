/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const timestamp = require("time-stamp");
const { post } = require("snekfetch");
const path = require("path");
const fs = require("fs");

/**
 * Logs command constructor.
 * 
 * @description Gives you the bot logs.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Logs extends Command {
	constructor (...args) {
		super(...args);
		this.content = fs.readFileSync(path.join(process.cwd(), `./logs/log-${timestamp(config.logDateFormat)}.log`), "utf8");
		this.Data = {
			maintainerOnly: true,
			aliases: ["loog"],
			usage: prefix => `${prefix}logs`,
			info: "Gives you the bot logs.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Uploads a GitHub gist with the logs.
	 * 
	 * @async
	 * @function upload
	 * @param {Object}
	 * @returns {Object} - The ID and URL of the Gist.
	 */
	async upload ({ file = "text.log" } = {}) {
		let res;
		let array = [
			this.client.token.escapeRegex(),
			config.botToken.escapeRegex(),
			config.botToken.replace(/^.{48}/g, "*************"),
			config.webhookToken.escapeRegex(),
			config.webhookID.escapeRegex(),
			config.dbUrl.escapeRegex(),
		];
		let regex = new RegExp(array.join("|"), "g");

		try {
			res = await post("https://api.github.com/gists").set({
				"User-Agent": "Tensa (https://github.com/oskikiboy/Tensa)",
				Accept: "application/json",
				"Content-Type": "application/json",
			}).send({
				description: `Tensa Logs`,
				public: false,
				files: {
					[file]: {
						content: this.content.replace(regex, "(╯°□°）╯︵ ┻━┻"),
					},
				},
			});
		} catch (err) {
			throw err;
		}

		return {
			id: res.body.id,
			url: res.body.html_url,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		let ma;

		try {
			msg.react("✅");
			if (msg.channel.type === "text") {
				let m = await msg.channel.send({
					embed: {
						color: this.client.Constants.embedColours.SUCCESS,
						description: `**${msg.author.username}, check your DM's!**`,
						thumbnail: {
							url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
						},
					},
				});

				setTimeout(() => {
					m.delete();
				}, 4000);
			}

			ma = await msg.author.send({
				embed: {
					description: "Posting gist...",
					color: this.client.Constants.embedColours.LOADING,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
					},
				},
			});

			let data = await this.upload();
			ma.edit({
				embed: {
					color: this.client.Constants.embedColours.INFO,
					description: `You can view the logs [here](${data.url})`,
				},
			});
		} catch (err) {
			this.log.error(`Error executing logs command:\n ${err}`);
			ma.edit({
				embed: {
					color: this.client.Constants.embedColours.ERROR,
					description: `Oh no, an unknown error has occoured!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
					},
				},
			});
		}
	}
};

