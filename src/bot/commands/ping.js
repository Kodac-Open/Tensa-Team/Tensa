/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Ping command constructor.
 * 
 * @description Tells you the bot ping.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Ping extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["pang"],
			usage: prefix => `${prefix}ping`,
			info: "Tells you the bot ping.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg }) {
		let hrstart = process.hrtime();
		let pingTime = (Date.now() - msg.createdTimestamp) / 2;

		msg.channel.send({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: `The current ping is **${pingTime.toFixed(1)}ms**`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/418874757740625929/ping.png",
				},
				footer: {
					text: `Bot Latency: ${Math.floor(this.client.ping)}ms | Execution time: ${process.hrtime(hrstart)[0]}s ${Math.floor(process.hrtime(hrstart)[1] / 1000000)}ms`,
				},
			},
		});
	}
};
