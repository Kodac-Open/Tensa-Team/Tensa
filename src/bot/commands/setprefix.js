/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * SetPrefix command constructor.
 * 
 * @description Sets the bot prefix.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class SetPrefix extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["prefix", "prefixset"],
			usage: prefix => `${prefix}setprefix <prefix>`,
			info: "Sets the bot prefix.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Settings",
			hidden: false,
		};
	}

	/**
	 * Runs if setting the prefix was succesful
	 * @function success
	 * @param {Object} msg
	 * @param {String} suffix
	 */
	success (msg, suffix) {
		msg.channel.send({
			embed: {
				color: this.client.Constants.embedColours.SUCCESS,
				description: `The guild prefix is now **${suffix}**`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
				},
				footer: {
					text: "These changes may take a few seconds to take effect!",
				},
			},
		});
	}

	/**
	 * Runs a check, if it returns true the command will run.
	 * 
	 * @function requirements
	 * @param {Object}
	 * @returns {Boolean}
	 */
	requirements ({ msg, suffix }) {
		if (!suffix || !msg.guild) return false;
		return true;
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg, suffix }) {
		if (msg.member.hasPermission("MANAGE_GUILD") || !this.client.checkforMaintainer(msg.author.id)) {
			this.client.setPrefix(msg.guild, suffix);
			this.success(msg, suffix);
		} else {
			return this.notMaintainer({ msg: msg });
		}
	}

	requirementsFail ({ msg }) {
		if (!msg.guild) {
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.WARN,
					description: "Please execute this command in a guild!",
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",
					},
				},
			});
		} else {
			msg.channel.send({
				embed: this.client.Constants.embeds.argsErr,
			});
		}
	}
};
