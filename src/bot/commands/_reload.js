/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Reload command constructor.
 * 
 * @description Reloads a/all command(s).
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
exports.run = class Reload extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: true,
			aliases: ["r"],
			usage: prefix => `${prefix}reload [flags] [cmd]`,
			info: "Reloads a/all command(s).",
			name: this.constructor.name.toLowerCase(),
			catagory: "Utility",
			hidden: false,
		};
	}

	// Very messy, redoing.
};
