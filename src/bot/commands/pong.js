/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Pong command constructor.
 * 
 * @description Tells you the bot pong.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Pong extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["po0ng"],
			usage: prefix => `${prefix}pong`,
			info: "Tells you the bot pong.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Fun",
			hidden: true,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg }) {
		msg.channel.send(":clown: You've got it the wrong way round!");
	}
};
