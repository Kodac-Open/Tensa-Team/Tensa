/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * SetGame command constructor.
 * 
 * @description Sets the bot game.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class SetGame extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: true,
			aliases: ["game", "sg", "sgame"],
			usage: prefix => `${prefix}setgame [game]`,
			info: "Sets the bot game.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Utility",
			hidden: false,
		};
	}

	/**
	 * Runs a check, if it returns true the command will run.
	 * 
	 * @function requirements
	 * @param {Object}
	 * @returns {Boolean}
	 */
	requirements ({ suffix }) {
		if (!suffix) return false;
		return true;
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg, suffix }) {
		suffix = suffix.split(" ");
		if (suffix[0] === "-d" || suffix[0] === "--defualt") {
			this.client.SetGame();
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.SUCCESS,
					description: `Successfuly set the game to **${prefix}help | Serving ${this.client.users.size} users** (Default)!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
					},
				},
			});
		} else {
			this.client.user.setActivity(suffix.join(" "), { type: "PLAYING" });
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.SUCCESS,
					description: `Successfuly set the game to **${suffix.join(" ")}**`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
					},
				},
			});
		}
	}

	/**
	 * Runs when the requirements fail.
	 * 
	 * @async
	 * @function requirementsFail
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async requirementsFail ({ msg }) {
		let m = await msg.channel.send({
			embed: this.client.Constants.embeds.argsErr,
		});

		setTimeout(() => {
			m.delete();
		}, 4000);
	}
};

