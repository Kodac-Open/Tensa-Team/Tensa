/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Hex command constructor.
 * 
 * @description Shows you the look of a color hex.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Hex extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["colour", "he"],
			usage: prefix => `${prefix}hex <colour>`,
			info: "Shows you the look of a color hex.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Utility",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg, suffix }) {
		let hex = suffix.replace("#", "").replace("0x", "");

		if (hex.length === 3) {
			hex = `${hex}${hex}`;
		}

		hex = hex.replace("000000", "010101");

		let dhex = `0x${hex}`;

		dhex = parseInt(dhex);

		if (/[0-9A-F]{6}$/i.test(hex)) {
			msg.channel.send({
				embed: {
					color: dhex,
					description: `\`#${hex.toUpperCase()}\``,
				},
			});
		} else {
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.WARN,
					description: `That doesn't look like a valid hex colour code!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",
					},
				},
			});
		}
	}
};
