/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Zucc command constructor.
 * 
 * @description Tells you about...The spy.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Ping extends Command {
  constructor(...args) {
    super(...args);
    this.Data = {
      maintainerOnly: false,
      aliases: ["spy", "facebook", "lizzard"],
      usage: prefix => `${prefix}zucc`,
      info: "Tells you about...The spy.",
      name: this.constructor.name.toLowerCase(),
      catagory: "Fun",
      hidden: false,
    };
  }

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
  run({ msg }) {
    msg.channel.send("Idk");
  }
};
