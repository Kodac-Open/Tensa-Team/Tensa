/* eslint-disable */
// When creating a real command remeber to reomve the /* eslint-disable */ line (and this line) and rename the constuctor from _ExampleCommand to something else.

/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Example command, explaining what everything does!
 */

 /**
 * Example command constructor.
 * 
 * @description An example command. - Gives you examples of functions and where they should go.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
exports.run = class _ExampleCommand extends Command {
	/**
	 * This is optional, but if you want to define a variable per command,
	 * you can use this. Useful for things like role IDs
	 * To access the bot variable, use `this.client`
	 */
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: true,
			aliases: ["ex"],
			usage: prefix => `${prefix}_example`,
			info: "An example command.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Utility",
			hidden: true,
		};
		this.exampleVariable = "Hello!";
	}

	/**
	 * Using this, the bot will auto check if some conditions match.
	 * For instance, you can check if the msg member has a specific role!
	 * It MUST return a boolean (true / false)
	 * By default, it returns true
	 */
	requirements ({ msg, suffix }) {
		return true;
	}

	/**
	 * This function will get called whenever the requirement fails. (AKA it returns false)
	 * Use this for instance to return an error message
	 */
	requirementsFail ({ msg, suffix }) {
		msg.reply(`No!`);
	}

	/**
	 * The main function of the command
	 * If the requirements pass, this function will be called
	 * Do all your command magic here!
	 */
	async run ({ msg, suffix }) {
		// Do things here!
	}

	/**
	 * Simple function that gets called if the user isn't a maintainer
	 * Its optional 
	 */
	noMaintainer ({ msg, suffix }) {
		// This will run if the user tries to run a maintainer-only command but isn't a maintainer
	}
};
