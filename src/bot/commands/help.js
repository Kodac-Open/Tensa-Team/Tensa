/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Help command constructor.
 * 
 * @description Gives help for the bot!
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Help extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["h", "he", "halp", "helpé", "helpme", "commands"],
			usage: prefix => `${prefix}help [command]`,
			info: "Gives help for the bot!",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg, suffix, guildPrefix }) {
		const { commands, checkforMaintainer } = this.client;
		const data = [];

		const catagories = {};

		if (!checkforMaintainer(msg.author.id)) {
			commands.find(cmd => {
				catagories[cmd.Data.name].push({ name: cmd.Data.name, Data: cmd.Data });
			});
		} else {
			commands.find(cmd => {
				if (!cmd.Data.maintainerOnly || !cmd.Data.hidden) {
					catagories[cmd.Data.name].push({ name: cmd.Data.name, Data: cmd.Data });
				}
			});
		}

		if (!suffix) {
			data.push(`:notepad_spiral: **Here's a list of all my commands:**\n\n`);
			data.push(`**Command syntax is as follows**\n\nOptional argumens: [],\nCompulsory arguments: <>`);
			Object.entries(catagories).forEach(e => {
				data.push(`\n\n__**${e[0]}**__${this.newLine}`);
				let num = 0;
				e[1].forEach(v => {
					if (num === e[1].length) {
						data.push(`**${guildPrefix + v.name}** | Usage: ${v.Data.usage(guildPrefix)} | Info: ${v.Data.info}`);
					} else {
						data.push(`**${guildPrefix + v.name}** | Usage: ${v.Data.usage(guildPrefix)} | Info: ${v.Data.info}${this.newLine}`);
					}
				});
			});
		} else {
			if (!commands.has(suffix)) {
				return msg.reply("That's not a valid command!");
			}

			const command = commands.get(suffix);

			data.push(`**Name:** ${command.Data.name}${this.newLine}`);

			if (command.Data.description) data.push(`**Description:** ${command.Data.description}${this.newLine}`);
			if (command.Data.aliases) data.push(`**Aliases:** ${command.Data.aliases.join(`, `)}${this.newLine}`);
			if (command.Data.usage) data.push(`**Usage:** ${command.Data.usage(guildPrefix)}`);
		}

		msg.react("✅");

		if (msg.channel.type === "text") {
			let m = await msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.SUCCESS,
					description: `**${msg.author.username}, check your DM's!**`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
					},
				},
			});

			setTimeout(() => {
				m.delete();
			}, 2000);
		}

		msg.author.send({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: data.join(``),
				footer: {
					text: `You can send \`${guildPrefix}help [command name]\` to get info on a specific command!`,
				},
			},
		});
	}
};

