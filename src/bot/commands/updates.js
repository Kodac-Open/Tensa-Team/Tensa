/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const { get } = require("snekfetch");

/**
 * Updates command constructor.
 * 
 * @description Provides bot updates.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Updates extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["up", "latest", "u"],
			usage: prefix => `${prefix}updates`,
			info: "Provides bot updates.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Gets the commits from Github
	 * 
	 * @async
	 * @function getCommits
	 * @returns {Array} commits - The commits in a form ready for Discord to use.
	 * @throws {Object|String} - err
	 */
	async getCommits () {
		let req;

		try {
			req = await get("https://api.github.com/repos/oskikiboy/Tensa/commits").set({
				"User-Agent": "Tensa (https://github.com/oskikiboy/Tensa)",
				Accept: "application/json",
				"Content-Type": "application/json",
			});

			let commits = [];
			let arrayNum = 0;

			req.body.splice(0, 9).forEach(c => {
				arrayNum++;
				var value = "";
				if (arrayNum === 9) {
					value = `View commit [here](${c.html_url})`;
				} else {
					value = `View commit [here](${c.html_url})${this.newLine}`;
				}
				commits.push({
					name: `${c.commit.message.replace(":memo:", ":pencil:").replace(":rotating_light", ":rotating_light:").replace(":zap", ":zap:")}`,
					value: value,
				});
			});

			return commits;
		} catch (err) {
			throw err;
		}
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		let m;
		try {
			m = await msg.channel.send({
				embed: {
					description: "Getting data...",
					color: this.client.Constants.embedColours.LOADING,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
					},
				},
			});

			let compiledCommits = await this.getCommits();
			m.edit({
				embed: {
					description: `All the latest commits from the **Tensa** [Github](${this.gitUrl})${this.newLine}`,
					fields: compiledCommits,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/410318160630120449/GitHub.png",
					},
					color: this.client.Constants.embedColours.INFO,
				},
			});
		} catch (err) {
			this.log.error(`Error executing updates command:\n ${err}`);
			m.edit({
				embed: {
					color: this.client.Constants.embedColours.ERROR,
					description: `Oh no, an unknown error has occoured!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
					},
				},
			});
		}
	}
};

