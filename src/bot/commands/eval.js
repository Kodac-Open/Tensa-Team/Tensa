/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Eval command constructor.
 * 
 * @description Evaluates arbitrary JavaScript.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Eval extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: true,
			aliases: ["ev"],
			usage: prefix => `${prefix}eval <code>`,
			info: "Evaluates arbitrary JavaScript.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Utility",
			hidden: false,
		};
	}

	/**
	 * Runs a check, if it returns true the command will run.
	 * 
	 * @function requirements
	 * @param {Object}
	 * @returns {Boolean}
	 */
	requirements ({ suffix }) {
		if (!suffix) return false;
		return true;
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg, suffix }) {
		let hrstart = process.hrtime();
		try {
			if (suffix.startsWith("```js") && suffix.endsWith("```")) suffix = suffix.substring(5, suffix.length - 3);
			const asyncify = code => `(async () => {\nreturn ${code.trim()}\n})()`;
			let result = await eval(asyncify(suffix));
			if (typeof result !== "string") result = require("util").inspect(result, false, 1);
			let array = [
				this.client.token.escapeRegex(),
				config.botToken.escapeRegex(),
				config.webhookToken.escapeRegex(),
				config.webhookID.escapeRegex(),
				config.dbUrl.escapeRegex(),
			];
			let regex = new RegExp(array.join("|"), "g");
			result = result.replace(regex, "(╯°□°）╯︵ ┻━┻");
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.SUCCESS,
					title: `Evaluated successfuly!\n`,
					description: `\`\`\`js\n${result}\`\`\``,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
					},
					footer: {
						text: `Execution time: ${process.hrtime(hrstart)[0]}s ${Math.floor(process.hrtime(hrstart)[1] / 1000000)}ms`,
					},
				},
			});
		} catch (err) {
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.ERROR,
					title: `Couldn't evaluate!\n`,
					description: `\`\`\`js\n${err.stack}\`\`\``,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
					},
					footer: {
						text: `Execution time: ${process.hrtime(hrstart)[0]}s ${Math.floor(process.hrtime(hrstart)[1] / 1000000)}ms`,
					},
				},
			});
		}
	}

	/**
	 * Runs when the requirements fail.
	 * 
	 * @async
	 * @function requirementsFail
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async requirementsFail ({ msg }) {
		let m = await msg.channel.send({
			embed: this.client.Constants.embeds.argsErr,
		});

		setTimeout(() => {
			m.delete();
		}, 4000);
	}
};
