/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const { get } = require("snekfetch");

/**
 * Cryptocurrency command constructor.
 * 
 * @description Gets info on a cryptocurrency.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Crypto extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["cry", "cryptocurrency"],
			usage: prefix => `${prefix}crypto <crypto> <currency>`,
			info: "Gets info on a cryptocurrency.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Utility",
			hidden: false,
		};
	}

	/**
	 * Runs a check, if it returns true the command will run.
	 * 
	 * @function requirements
	 * @param {Object}
	 * @returns {Boolean}
	 */
	requirements ({ suffix }) {
		if (!suffix) return false;
		return true;
	}

	/**
	 * Gets stats of a cryptocurrency.
	 * 
	 * @async
	 * @function getCrypto
	 * @param {Array} - Parameters inputted when the command was run.
	 */
	async getCrypto (SA) {
		let res;
		try {
			res = await get(`https://api.cryptonator.com/api/ticker/${SA[0]}-${SA[1]}`);
		} catch (err) {
			throw err;
		}
		return res.body;
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg, suffix }) {
		let m = await msg.channel.send({
			embed: {
				description: "Getting data...",
				color: this.client.Constants.embedColours.LOADING,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
				},
			},
		});
		let suffixArray = suffix.split(" ");
		this.getCrypto(suffixArray).then(c => {
			if (c.success === false) {
				m.edit({
					embed: {
						color: this.client.Constants.embedColours.ERROR,
						title: `Oh no, we just got an error!`,
						description: `${c.error}.`,
						thumbnail: {
							url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
						},
					},
				});
			} else {
				m.edit({
					embed: {
						color: this.client.Constants.embedColours.INFO,
						description: `Here is the requested information on the requested CryptoCurrency (${c.ticker.base}):`,
						fields: [
							{
								name: "Value:",
								value: `${Math.round(c.ticker.price * 100) / 100} ${c.ticker.target}${this.newLine}`,
							},
							{
								name: "Change (Past Hour):",
								value: `${Math.round(c.ticker.change * 100) / 100} ${c.ticker.target}${this.newLine}`,
							},
							{
								name: "(Total) Trade Volume:",
								value: `${Math.round(c.ticker.volume * 100) / 100} ${c.ticker.target}`,
							},
						],
						thumbnail: {
							url: "https://cdn.discordapp.com/attachments/408878735136391168/414723479351328768/money.png",
						},
					},
				});
			}
		}).catch(err => {
			m.edit({
				embed: {
					color: this.client.Constants.embedColours.ERROR,
					description: `Oh no, an unknown error has occoured!`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
					},
				},
			});
			this.log.error(`Error executing cryptocurrency command:\n ${err}`);
		});
	}

	/**
	 * Runs when the requirements fail.
	 * 
	 * @async
	 * @function requirementsFail
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async requirementsFail ({ msg }) {
		let ma = await msg.channel.send({
			embed: this.client.Constants.embeds.argsErr,
		});
		setTimeout(() => {
			ma.delete();
		}, 4000);
	}
};
