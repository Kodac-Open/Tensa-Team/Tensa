/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Support command constructor.
 * 
 * @description Provides an invite to the bot support server.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Support extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["s", "sup"],
			usage: prefix => `${prefix}support`,
			info: "Provides an invite to the bot support server.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		msg.react("✅");
		if (msg.channel.type === "text") {
			let m = await msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.SUCCESS,
					description: `**${msg.author.username}, check your DM's!**`,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408878837779398656/tick.png",
					},
				},
			});

			setTimeout(() => {
				m.delete();
			}, 4000);
		}

		msg.author.send({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: `Join the Tensa support guild [here](https://discord.gg/pRqk8dP)`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/412898703754657793/clip.png",
				},
			},
		});
	}
};

