/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const { get } = require("snekfetch");

/**
 * Meme command constructor.
 * 
 * @description Gives you a random meme.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Meme extends Command {
  constructor(...args) {
    super(...args);
    this.Data = {
      maintainerOnly: false,
      aliases: ["funny"],
      usage: prefix => `${prefix}meme`,
      info: "Gives you a random meme.",
      name: this.constructor.name.toLowerCase(),
      catagory: "Fun",
      hidden: false,
    };
  }

	/**
	 * Sends a request to an API to get a meme.
	 * 
	 * @async
	 * @function getMeme
	 * @returns {String}
	 */
  async getMeme() {
    let res;
    try {
      res = await get("https://meme-api.explosivenight.us/v1/random/?type=json");
    } catch (err) {
      throw err;
    }
    res = res.body.url;
    if (res.includes(".mp4")) {
      return this.getMeme();
    } else {
      return res
    }
  }

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
  async run({ msg }) {
    let m = await msg.channel.send({
      embed: {
        description: "Getting meme...",
        color: this.client.Constants.embedColours.LOADING,
        thumbnail: {
          url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
        },
      },
    });
    let meme = await this.getMeme().catch(err => {
      m.edit({
        embed: {
          color: this.client.Constants.embedColours.ERROR,
          description: `Oh no, an unknown error has occoured!`,
          thumbnail: {
            url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
          },
        },
      });
      this.log.error(`Error executing memes command:\n ${err}`);
    });
    m.edit({
      embed: {
        color: this.client.Constants.embedColours.INFO,
        description: `Here's an edgy meme.`,
        thumbnail: {
          url: "https://cdn.discordapp.com/attachments/408878735136391168/437073410997223434/sssssss.png",
        },
        image: {
          url: meme,
        },
      },
    });
  }
};
