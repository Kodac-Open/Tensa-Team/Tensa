/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Lund command constructor.
 * 
 * @description Predicts your penis size.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Lund extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["penis", "lun", "lundi"],
			usage: prefix => `${prefix}lund [@user]`,
			info: "Predicts your penis size.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Fun",
			hidden: false,
		};
	}

	/**
	 * Determines the size (randomly).
	 * 
	 * @function determine
	 * @param {Number} amount 
	 * @param {String} id
	 * @returns {String} - The size.
	 */
	determine (amount, id = "") {
		if (id && id === "193972602392281088") {
			return `8===================D`;
		} else {
			return `8${"=".repeat(Math.floor(Math.random() * amount) + 1)}D`;
		}
	}

	/**
	 * Gets information on the user based on the parameters passed.
	 * 
	 * @function testUser
	 * @param {Object}
	 * @returns {String} - The final judgement.
	 */
	testUser ({ msg, suffix }) {
		if (suffix && suffix.includes("<@")) {
			let name = msg.mentions.users.array()[0].username;
			let id = msg.mentions.users.array()[0].id;

			return name.endsWith("s") ? `${name}' lund size is ${this.determine(12, id)}` : `${name}'s lund size is ${this.determine(12, id)}`;
		} else {
			return `Your lund size is ${this.determine(12, msg.author.id)}`;
		}
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run (message) {
		message.msg.channel.send({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: this.testUser(message),
			},
		});
	}
};

