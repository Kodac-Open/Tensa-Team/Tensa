/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const { get } = require("snekfetch");

/**
* Cat command constructor.
* 
* @description Gives you a random picture of a cat.
* 
* @constructor
* @augments Command - Extends to the base command.
* @param {Object} client - Passes the client for use in commands.
*/
module.exports = class Cat extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["cac", "meow"],
			usage: prefix => `${prefix}cat`,
			info: "Gives you a random picture of a cat.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Fun",
			hidden: false,
		};
	}

	/**
	 * Sends a request to an API to get a picture of a cat.
	 * 
	 * @async
	 * @function getCat
	 * @returns {String}
	 */
	async getCat () {
		let res;
		try {
			res = await get("https://random.cat/meow");
		} catch (err) {
			throw err;
		}
		return res.body.file;
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		let m = await msg.channel.send({
			embed: {
				description: "Getting cat...",
				color: this.client.Constants.embedColours.LOADING,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
				},
			},
		});
		let d = await this.getCat();
		m.edit({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: `Here's a cute kitty.`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/410355296989478912/cat.png",
				},
				image: {
					url: d,
				},
			},
		});
	}
};
