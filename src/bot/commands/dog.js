/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");
const { get } = require("snekfetch");

/**
 * Dog command constructor.
 * 
 * @description Gives you a random picture of a dog.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Dog extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["do", "woof"],
			usage: prefix => `${prefix}dog`,
			info: "Gives you a random picture of a dog.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Fun",
			hidden: false,
		};
	}

	/**
	 * Sends a request to an API to get a picture of a dog.
	 * 
	 * @async
	 * @function getDog
	 * @returns {String}
	 */
	async getDog () {
		let res;
		try {
			res = await get("https://random.dog/woof");
		} catch (err) {
			throw err;
		}
		if (res.text.includes(".mp4")) {
			return this.getDog();
		} else {
			return `https://random.dog/${res.text}`;
		}
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		let m = await msg.channel.send({
			embed: {
				description: "Getting dog...",
				color: this.client.Constants.embedColours.LOADING,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
				},
			},
		});
		let d = await this.getDog();
		m.edit({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: `Here's a cute doggy.`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/410337670120865793/dog.png",
				},
				image: {
					url: d,
				},
			},
		});
	}
};
