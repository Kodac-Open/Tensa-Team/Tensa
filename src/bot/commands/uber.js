/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
* Uber command constructor.
* 
* @description Orders an Uber for you.
* 
* @constructor
* @augments Command - Extends to the base command.
* @param {Object} client - Passes the client for use in commands.
*/
module.exports = class Uber extends Command {
	constructor (...args) {
		super(...args);

		// Command specific colours.
		this.client.Constants.embedColours.UBER = 0x1fbad6;
		this.client.Constants.embedColours.UBER_NONE = 0x222233;

		this.Data = {
			maintainerOnly: false,
			aliases: ["Uber", "taxi"],
			usage: prefix => `${prefix}uber`,
			info: "Orders an Uber for you.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Fun",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		let m = await msg.channel.send({
			embed: {
				description: "Ordering uber...",
				color: this.client.Constants.embedColours.UBER,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",
				},
			},
		});

		setTimeout(() => {
			m.edit({
				embed: {
					color: this.client.Constants.embedColours.UBER_NONE,
					description: "You actually thought this would get you an Uber?",
				},
			});
		}, 5000);
	}
};
