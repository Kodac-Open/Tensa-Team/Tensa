/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Profile command constructor.
 * 
 * @description Tells you information about a user or yourself.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Profile extends Command {
	
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["prof"],
			usage: prefix => `${prefix}profile [user]`,
			info: "Tells you information about a user or yourself.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	/**
	 * Runs the command
	 * 
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg, suffix }) {
		let user = !suffix ? msg.author : suffix.includes("<@") && msg.mentions ? msg.mentions.users.array().length === 1 ? msg.mentions.users.array()[0] : msg.mentions.users.array() : msg.author;

		if (Array.isArray(user)) {
			user.forEach(u => {
				let createdUnix = new Date(u.createdTimestamp).toUTCString();
				let profile = `**:name_badge: Username:** \`${u.username}\`\n
				\n**:control_knobs: Discriminator:** \`${u.discriminator}\`\n
				\n**:id: ID:** \`${u.id}\`\n
				\n**:calendar_spiral: Creation date:** \`${createdUnix}\`\n
				\n**:robot: Bot?** \`${u.bot ? "Yes" : "No"}\``;

				msg.channel.send({
					embed: {
						color: this.client.Constants.embedColours.INFO,
						description: profile,
						thumbnail: {
							url: u.displayAvatarURL(),
						},
					},
				});
			});
		} else {
			let createdUnix = new Date(user.createdTimestamp).toUTCString();
			let profile = `**:name_badge: Username:** \`${user.username}\`\n
			\n**:control_knobs: Discriminator:** \`${user.discriminator}\`\n
			\n**:id: ID:** \`${user.id}\`\n
			\n**:calendar_spiral: Creation date:** \`${createdUnix}\`\n
			\n**:robot: Bot?** \`${user.bot ? "Yes" : "No"}\``;

			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.INFO,
					description: profile,
					thumbnail: {
						url: user.displayAvatarURL(),
					},
				},
			});
		}
	}
};
