/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 * Emojis command constructor.
 * 
 * @description Tells you the emojis on the guild.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Object} client - Passes the client for use in commands.
 */
module.exports = class Emojis extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["emoji"],
			usage: prefix => `${prefix}emojis`,
			info: "Tells you the emojis on the guild.",
			name: this.constructor.name.toLowerCase(),
			catagory: "Info",
			hidden: false,
		};
	}

	requirements ({ msg }) {
		if (!msg.guild) return false;
		return true;
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async run ({ msg }) {
		const emojiList = msg.guild.emojis.map(e => e.toString()).join(" ");
		if (!emojiList) {
			let m = await msg.channel.send({
				embed: {
					description: `You don't have any **emojis** on this guild!`,
					color: this.client.Constants.embedColours.WARN,
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",
					},
				},
			});
			setTimeout(() => {
				m.delete();
			}, 2000);
		} else {
			msg.channel.send({
				embed: {
					description: `List of **emojis** in this guild:\n${this.newLine}${emojiList}`,
					color: this.client.Constants.embedColours.INFO,
				},
			});
		}
	}

	/**
	 * Runs when the requirements fail.
	 * 
	 * @function requirementsFail
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	requirementsFail ({ msg }) {
		if (!msg.guild) {
			msg.channel.send({
				embed: {
					color: this.client.Constants.embedColours.WARN,
					description: "Please execute this command in a guild!",
					thumbnail: {
						url: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",
					},
				},
			});
		}
	}
};
