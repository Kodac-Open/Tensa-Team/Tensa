/* eslint-disable no-useless-constructor */

const { Command } = require("../../Internals/");

/**
 *  Command constructor.
 * 
 * @description Gets magic 8ball to answer your question.
 * 
 * @constructor
 * @augments Command - Extends to the base command.
 * @param {Array} client - Passes the client for use in commands.
 */
module.exports = class EightBall extends Command {
	constructor (...args) {
		super(...args);
		this.Data = {
			maintainerOnly: false,
			aliases: ["eightball", "ball", "8"],
			usage: prefix => `${prefix}8ball <question>`,
			info: "Gets magic 8ball to answer your question.",
			name: "8ball",
			catagory: "Fun",
			hidden: false,
		};
		this.responses = [
			"Unclear, ask again later",
			"Soon",
			"Yes",
			"Absolutely!",
			"Never",
			"Na you are too stupid!",
			"Maybe if you get your ma to do it for you",
			"Damn you are a bright spark",
			"You obviously didn't have many friends if your asking that question!",
			"Magic 8-ball is currently unavailable, please leave a message after the tone. \\*beep\\*",
			"When you are ready",
			"Hopefully",
			"Hopefully not",
			"Oh my, why would you even ask that?",
			"What kind of a question is that?",
			"Over my dead body! (~~And sure I'm a bot~~)",
			"Haha, funny joke**",
		];
	}

	/**
	 * Runs a check, if it returns true the command will run.
	 * 
	 * @function requirements
	 * @param {Object}
	 * @returns {Boolean}
	 */
	requirements ({ suffix }) {
		if (!suffix) return false;
		return true;
	}

	/**
	 * Runs the command
	 * 
	 * @async
	 * @function run
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	run ({ msg }) {
		let answer = this.responses[Math.floor(Math.random() * this.responses.length)];
		msg.channel.send({
			embed: {
				color: this.client.Constants.embedColours.INFO,
				description: `:8ball: ${answer}`,
			},
		});
	}

	/**
	 * Runs when the requirements fail.
	 * 
	 * @async
	 * @function requirementsFail
	 * @param {Object} - Parameters to be passed to the bot.
	 */
	async requirementsFail ({ msg }) {
		let m = await msg.channel.send({ embed: this.client.Constants.embeds.argsErr });
		setTimeout(() => {
			m.delete();
		}, 2000);
	}
};
