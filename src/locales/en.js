module.exports = {
	other: {
		checkDMS: author => `**${author}, check your DM's!**`,
		getData: "Getting data...",
	},

	errors: {
		blocked: "Uh-oh it seems that you were blocked from using the bot!",
		cmdNotExist: "This command **does not exist**!",
		uErr: "Oh no, an unknown error has occoured!",
		argsErr: "This command requires you to **supply arguments**!",
	},

	commands: {
		"8ball": {
			reply: answer => `:8ball: ${answer}`,
			responses: [
				"Unclear, ask again later",
				"Soon",
				"Yes",
				"Absolutely!",
				"Never",
				"Na you are too stupid!",
				"Maybe if you get your ma to do it for you",
				"Damn you are a bright spark",
				"You obviously didn't have many friends if your asking that question!",
				"Magic 8-ball is currently unavailable, please leave a message after the tone. \\*beep\\*",
				"When you are ready",
				"Hopefully",
				"Hopefully not",
				"Oh my, why would you even ask that?",
				"What kind of a question is that?",
				"Over my dead body! (~~And sure I'm a bot~~)",
				"Haha, funny joke**",
			],
		},

		about: (all, os, version) => ({
			description: `**${all.client.user.username}**, version ${all.tVer}, is a powerful Discord utility bot. Written by **${all.client.users.get("193972602392281088").tag}**${all.newLine}`,
			fields: [
				{
					name: "Bot info:",
					value: `${all.client.user.tag} (${all.client.user.id}).${all.newLine}`,
				},
				{
					name: "Commands Executed:",
					value: `${all.client.cmdExec}.${all.newLine}`,
				},
				{
					name: "Github:",
					value: `You can find the github, [here](${all.gitUrl}).${all.newLine}`,
				},
				{
					name: "Library:",
					value: `Discord.JS, Version ${version}.${all.newLine}`,
				},
				{
					name: "System info:",
					value: `${process.platform}-${process.arch} with ${process.release.name} version ${process.version.slice(1)}.${all.newLine}`,
				},
				{
					name: "Process info (PID):",
					value: `${process.pid}.${all.newLine}`,
				},
				{
					name: "Process memory usage:",
					value: `${Math.ceil(process.memoryUsage().heapTotal / 1000000)} MB.${all.newLine}`,
				},
				{
					name: "System memory usage:",
					value: `${Math.ceil((os.totalmem() - os.freemem()) / 1000000)} of ${Math.ceil(os.totalmem() / 1000000)} MB.`,
				},
			],
		}),

		crypto: (c, newLine) => ({
			description: `Here is the requested information on the requested CryptoCurrency (${c.ticker.base}):`,
			fields: [
				{
					name: "Value:",
					value: `${Math.round(c.ticker.price * 100) / 100} ${c.ticker.target}${newLine}`,
				},
				{
					name: "Change (Past Hour):",
					value: `${Math.round(c.ticker.change * 100) / 100} ${c.ticker.target}${newLine}`,
				},
				{
					name: "(Total) Trade Volume:",
					value: `${Math.round(c.ticker.volume * 100) / 100} ${c.ticker.target}`,
				},
			],
			cryErr: `${c.error}.`,
		}),

		dog: {
			description: `Here's a cute doggy.`,
			getData: "Getting dog...",
		},

		help: {
			all: v => ({
				l1: ":notepad_spiral: **Here's a list of all my commands:**",
				l2: "**Command syntax is as follows**\n\nOptional argumens: [],\nCompulsory arguments: <>",
				cmd: `**${prefix + v.name}** | Usage: ${v.Data.usage} | Info: ${v.Data.info}`,
			}),
			individual: (command, newLine) => ({
				invalid: "That's not a valid command!",
				l1: `**Name:** ${command.Data.name}${newLine}`,
				l2: `**Description:** ${command.Data.description}${newLine}`,
				l3: `**Aliases:** ${command.Data.aliases.join(`, `)}${newLine}`,
				l4: `**Usage:** ${command.Data.usage}`,
			}),
			footer: prefix => `You can send \`${prefix}help [command name]\` to get info on a specific command!`,
		},

		hex: {
			hexErr: "That doesn't look like a valid hex colour code!",
		},

		lund: (name, determine, id) => ({
			r1: name.endsWith("s") ? `${name}' lund size is ${determine(12, id)}` : `${name}'s lund size is ${determine(12, id)}`,
			r2: `Your lund size is ${determine(12, id)}`,
		}),

		ping: (ping, pingTime, hrstart) => ({
			description: `The current ping is **${pingTime.toFixed(1)}ms**`,
			footer: `Bot Latency: ${Math.floor(ping)}ms | Execution time: ${process.hrtime(hrstart)[0]}s ${Math.floor(process.hrtime(hrstart)[1] / 1000000)}ms`,
		}),

		pong: ":clown: You've got it the wrong way round!",

		setprefix: {
			description: prefix => `The guild prefix is now **${prefix}**`,
			guildErr: "Please execute this command in a guild!",
		},

		support: `Join the Tensa support guild [here](https://discord.gg/pRqk8dP)`,

		updates: gitUrl => `All the latest commits from the **Tensa** [Github](${gitUrl})`,
	},
};
