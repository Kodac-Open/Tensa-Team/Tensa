/**
 * Tensa
 * 
 * @version 1.2.1
 * @file Initialises shards.
 * @author oskikiboy
 */

// Get config.
global.config = require("../configurations/config");

// Get the Sharding Manager.
const { ShardingManager } = require("./Internals/");

// Create an instance of the Sharding Manager class.
const Manager = new ShardingManager("./src/Tensa.js");

// Spawn the defined amount of shards.
if (config.shardAmount === "none") {
	require("./Tensa");
} else {
	Manager.spawn(config.shardAmount === "cpu" ? require("os").cpus().length : config.shardAmount, {
		token: config.botToken,
	});
	
	require("./modules/longtaskWorker");

	process.on("unhandledRejection", reason => {
		// eslint-disable-next-line no-console
		console.error(`An unexpected error occurred.\n`, reason);
	});

	process.on("uncaughtException", err => {
		// eslint-disable-next-line no-console
		console.error(`An unexpected and unknown error occurred.\n`, err);
		process.exit(1);
	});
}
