/* eslint-disable */

/**
 * Navbar, background, mobile
 * 
 * @author oskikiboy & Acorn
 */

window.onload = () => {

    /** @description - Makes the nav expand on scoll. */

    let navMenu = document.getElementById("navMenu");
    let navBurger = document.getElementById("navMenuToggle");
    let navBtn = document.getElementById("nav-btn");

    wRs();

    window.onscroll = () => {
        if (window.scrollY > 60 || document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
            if (window.innerWidth >= 1006) {
                navMenu.style.padding = `0px`;
                navMenu.classList.add("is-scrolled");
                navBurger.style.color = "rgba(10,10,10,.9)";
                navBtn.classList.remove("is-outlined");
				navBtn.classList.remove("is-medium");
            }
        } else if (window.innerWidth > 1006) {
            navMenu.style.padding = `16px`;
            navMenu.classList.remove("is-scrolled");
            navBurger.style.color = "#fff";
            navBtn.classList.add("is-outlined");
            navBtn.classList.add("is-medium");
        }
    };

    /** @description - Makes the nav display correctly on mobile. */

    window.onresize = () => wRs()

    function wRs () {
        if (window.innerWidth < 1006) {
            navMenu.style.padding = `0px`;
            navMenu.classList.add("is-scrolled");
            navBurger.style.color = "rgba(10,10,10,.9)";
            navBtn.classList.remove("is-outlined");
			navBtn.classList.remove("is-medium");
			navBtn.style.width = `100%`;
        }
    }

    /** @description - Makes the navbar mobile menu expand when burger is clicked. */

    var navbarBurgers = Array.prototype.slice.call(document.querySelectorAll(".navbar-burger"), 0);

    if (navbarBurgers.length > 0) {
        navbarBurgers.forEach(el => {
            el.addEventListener("click", () => {
                let target = el.dataset.target;
                let _target = document.getElementById(target);
                el.classList.toggle("is-active");
                _target.classList.toggle("is-active");
            });
        });
    }

    /** @description - Makes the background change. (Disabled (on firefox) */

    /*if (!navigator.userAgent.toLowerCase().indexOf("firefox") > -1) {
        Array.prototype.recurse = function (callback) {
            let self = this, idx = 0;

            setInterval(() => {
                callback(self[idx], idx);
                idx = idx + 1 < self.length ? idx + 1 : 0;
            }, 5000);
        };

        backgrounds.recurse(item => {
            document.getElementById("bg").style.backgroundImage = `url(${baseUrl + item})`;
        });
    }*/
};
