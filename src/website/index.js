/**
 * @file Webserver for Tensa.
 * @author oskikiboy
 */

const { Util } = require("../Internals/");
const { readDir } = Util;
const path = require("path");

/**
 * Express server.
 * 
 * @constructor Server
 * @param {Object} client
 */
class Server {
	constructor (client) {
		// Discord bot Client.
		this.client = client;

		// Initialises a logger instance.
		this.log = new logger("Website");

		// eslint-disable-next-line no-unused-vars
		this.loadedFiles = [];

		// Express.
		this.express = require("express");

		// Starts express.
		this.app = this.express();
	}

	/**
	 * Initialises the web interface.
	 * 
	 * @async - Uses async for promise.
	 * @function init
	 */
	async init () {
		// Allows static assets to be accessed from /static
		this.app.use("/static", this.express.static(`${__dirname}/static`));

		// Tells Express where to look for views.
		this.app.set("views", path.join(__dirname, "views"));

		// Set the view engine as EJS.
		this.app.set("view engine", "ejs");

		// Stop people from the hax
		this.app.use((req, res, next) => {
			res.setHeader("X-Powered-By", "Magic");
			next();
		});

		// Allows this to be accessed from any of the routes.
		this.app.set("all", this);

		// Load the routes.
		this.loadRoutes();

		// Listen on defined port.
		this.app.listen(config.httpPort, err => {
			if (err) {
				return this.log.crit(`Failed to bind webserver to port\n${err.stack}`);
			}
			this.log.suc(`Successfully started server...listening on port ${config.httpPort || 8081}`);
		});
	}

	/**
	 * @author oskikiboy & Acorn
	 */
	loadRoutes () {
		// Reads the directory.
		readDir(path.join(__dirname, "./routes")).then(files => {
			// Counts the files.
			let count = 0;
			this.loadedFiles = files;

			// For each loaded file, enable a route to be used in express.
			for (const file of files) {
				if (file.startsWith("_")) continue;
				this.app.use(require(`./routes/${file}`));
				count++;
			}

			// Determine to use plural based on the amount of routes.
			let plural = "";
			if (count > 1) plural = "s";
			this.log.info(`Loaded ${count} route file${plural}!`);

			// If a 404 is to occour.
			this.app.use((req, res) => {
				res.status(404).render("error", {
					url: "https://d1ig1wrexgcni.cloudfront.net/assets", code: 404, msg: "Page not found", location: "error", debug: "None",
				});
			});

			// If an error occours.
			this.app.use((err, req, res) => {
				this.log.error(`An error on the web interface occoured, ${err}`);
				if (err) {
					res.status(500).render("error", {
						url: "https://d1ig1wrexgcni.cloudfront.net/assets", code: 500, msg: "Internal Server Error", location: "error", debug: err,
					});
				}
			});
		});
	}
}

module.exports = Server;
