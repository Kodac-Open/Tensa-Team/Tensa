// Setting up
const express = require("express");
const app = express.Router();

/**
 * Get /
 * 
 * @async
 * @function get
 * @param {Object} req
 * @param {Object} res
 */
app.get("/", async (req, res) => {
	let all;
	try {
		all = req.app.get("all");
		res.render("index", {
			gitUrl: require("../../../package.json").repository.url.substring(0, require("../../../package.json").repository.url.indexOf(".git")),
			url: "https://d1ig1wrexgcni.cloudfront.net/assets",
			commandAmount: all.client.totalCommands,
			location: "index",
		});
	} catch (err) {
		all.log.error(`An error on the web interface occoured, ${err}`);
		res.status(500).render("error", {
			url: "https://d1ig1wrexgcni.cloudfront.net/assets", code: 500, msg: "Internal Server Error", location: "error", debug: err,
		});
	}
});

module.exports = app;
