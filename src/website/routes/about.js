// Setting up
const express = require("express");
const app = express.Router();

/**
 * Get /guilds
 * 
 * @async
 * @function get
 * @param {Object} req
 * @param {Object} res
 */
app.get("/about", async (req, res) => {
	let all;
	try {
		all = req.app.get("all");
		res.render("about", {
			url: "https://d1ig1wrexgcni.cloudfront.net/assets",
			location: "about",
			creatorAvatar: all.client.users.get("193972602392281088").displayAvatarURL(),
		});
	} catch (err) {
		all.log.error(`An error on the web interface occoured, ${err}`);
		res.status(500).render("error", {
			url: "https://d1ig1wrexgcni.cloudfront.net/assets", code: 500, msg: "Internal Server Error", location: "error", debug: err,
		});
	}
});

module.exports = app;
