// Setting up
const express = require("express");
const app = express.Router();

/**
 * Get /guilds
 * 
 * @async
 * @function get
 * @param {Object} req
 * @param {Object} res
 */
app.get("/guilds", async (req, res) => {
	let all;
	try {
		all = req.app.get("all");
		let guildList = config.shardAmount !== "none" ? [].concat(...await all.client.shard.broadcastEval("this.guilds")) : all.client.guilds;
		guildList.sort((a, b) => a.members.size - b.members.size);
		res.render("guilds", {
			url: "https://d1ig1wrexgcni.cloudfront.net/assets",
			servers: guildList,
			location: "guilds",
		});
	} catch (err) {
		all.log.error(`An error on the web interface occoured, ${err}`);
		res.status(500).render("error", {
			url: "https://d1ig1wrexgcni.cloudfront.net/assets", code: 500, msg: "Internal Server Error", location: "error", debug: err,
		});
	}
});

module.exports = app;
