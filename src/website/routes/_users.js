// Setting up
const express = require("express");
const app = express.Router();
const fs = require("fs");

/**
 * Writes blocked user to a file.
 * 
 * @function writeBlocked
 * @param {Number|String} input 
 */
function writeBlocked (input) {
	fs.readFile(`${process.cwd()}/configurations/blockedUsers.json`, (err, content) => {
		if (err) throw err;
		let parseJson = JSON.parse(content);

		parseJson.blockedUsers.push(input);

		// eslint-disable-next-line no-shadow
		fs.writeFile(`${process.cwd()}/configurations/blockedUsers.json`, JSON.stringify(parseJson), err => {
			if (err) throw err;
		});
	});
}

/**
 * Get /users
 * 
 * @async
 * @function get
 * @param {Object} req
 * @param {Object} res
 */
app.get("/users", async (req, res) => {
	let all = req.app.get("all");
	let userList = [].concat(...await all.client.shard.broadcastEval("this.users"));
	res.render("users", {
		url: "https://d1ig1wrexgcni.cloudfront.net/assets",
		servers: userList.sort(),
		location: "users",
	});
});

// eslint-disable-next-line no-shadow, no-unused-vars
app.post("/users/block", (req, res) => {
	writeBlocked();
});

module.exports = app;
