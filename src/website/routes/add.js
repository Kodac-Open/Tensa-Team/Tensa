// Setting up
const express = require("express");
const app = express.Router();

/**
 * Get /add
 * 
 * @async
 * @function get
 * @param {Object} req
 * @param {Object} res
 */
app.get("/add", (req, res) => {
	let all;
	try {
		all = req.app.get("all");
		res.redirect(`https://discordapp.com/api/oauth2/authorize?client_id=${all.client.user.id}&permissions=0&redirect_uri=${req.protocol}%3A%2F%2F${req.get("host").replace(":", "%3A")}&scope=bot`);
	} catch (err) {
		all.log.error(`An error on the web interface occoured, ${err}`);
		res.status(500).render("error", {
			url: "https://d1ig1wrexgcni.cloudfront.net/assets", code: 500, msg: "Internal Server Error", location: "error", debug: err,
		});
	}
});

module.exports = app;
