const { EventEmitter } = require("events");

const { WebhookClient } = require("discord.js");

const { init: pmx, probe } = require("pmx");

if (process.env.NODE_ENV === "production") {
	pmx({
		http: true,
		ignore_routes: [/socket\.io/],
		errors: true,
		custom_probes: true,
		network: true,
		ports: true,
	});
}

/**
 * Watcher constructor
 * 
 * @constructor
 * @param {Object} client - Discord.js Client.
 */
class Watcher {
	constructor (client) {
		this.client = client;
		if (probe) {
			this.probe = probe();
		}
		this.hook = new WebhookClient(config.webhookID, config.webhookToken);
		this.IPC = EventEmitter();

		this.handlePing();

		this.on("messages", msg => this.handleMessages(msg));
		this.on("guilds", msg => this.handleGuilds(msg));
	}

	/**
	 * Sets up a keymetrics histogram.
	 * 
	 * @function handleGuilds - Creates a histogram with the guild total, sends data to a webhook, updates every time a new guild is added.
	 * @param {String} msg 
	 */
	handleGuilds (msg) {
		this.hook.send();
		this.client.log.info(msg);
		if (this.probe) {
			this.probe.histogram({
				name: "Guild Total",
				measurement: "mean",
			});
		}
	}

	/** 
	 * Sets up a keymetrics histogram.
	 * 
	 * @function handlePing - Creates a histogram of the client ping, adds to it every 1 second.
	 */
	handlePing () {
		let histogram = this.probe.histogram({
			name: "Client ping",
			measurement: "mean",
		});

		let latency = 0;

		setInterval(() => {
			latency = this.client.ping;
			histogram.update(latency);
		}, 1000);
	}
}

module.exports = Watcher;
