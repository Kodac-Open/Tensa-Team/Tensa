/* eslint-disable no-console */

/**
 * @file Logger for Tensa.
 * @author oskikiboy
 */

// eslint-disable-next-line no-unused-vars
const colors = require("colors");
const timestamp = require("time-stamp");
const config = require("../../configurations/");

// Transports stuff.
const { Console } = require("console");
const fs = require("fs");
const path = require("path");

// Check if transports directory exsists.
if (!fs.existsSync(path.join(__dirname, "../../logs"))) {
	console.log(`[${"LOGGER".gray}] Creating log directory.`);
	fs.mkdirSync(path.join(__dirname, "../../logs"));
}

// Transports streams.
const output = fs.createWriteStream(path.join(__dirname, `../../logs/log-${timestamp(config.logDateFormat)}.log`), { flags: "a" });
const errorOutput = fs.createWriteStream(path.join(__dirname, `../../logs/err-${timestamp(config.logDateFormat)}.log`), { flags: "a" });

const logFile = new Console(output, errorOutput);
logFile.log("---------- START SESSION ----------");

function getTimestamp () {
	return `[${timestamp("HH:mm:ss".grey)}]`;
}

function getCleanTimestamp () {
	return `[${timestamp("HH:mm:ss")}]`;
}

class logger {
	constructor (proc) {
		if (proc) {
			this.proc = proc;
		} else {
			this.error("Please supply a process argument for the logger!");
			process.exit(-1);
		}
	}

	log (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.log(`${time} `, ...args);
		logFile.log(`${cleanstamp} `, ...args);
		return this;
	}

	suc (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.info(`${time} [${"SUCCESS".green}] [${this.proc}] - `, ...args);
		logFile.info(`${cleanstamp} [SUCCESS] [${this.proc}] - `, ...args);
		return this;
	}

	info (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.info(`${time} [${"INFO".blue}] [${this.proc}] - `, ...args);
		logFile.info(`${cleanstamp} [INFO] [${this.proc}] - `, ...args);
		return this;
	}

	silly (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.info(`${time} [${"SILLY".rainbow}] [${this.proc}] - `, ...args);
		logFile.info(`${cleanstamp} [SILLY] [${this.proc}] - `, ...args);
		return this;
	}

	dir (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.dir(`${time} [${"DIR".grey}] [${this.proc}] - `, ...args);
		logFile.dir(`${cleanstamp} [DIR] [${this.proc}] - `, ...args);
		return this;
	}

	warn (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.warn(`${time} [${"WARN".yellow}] [${this.proc}] - `, ...args);
		logFile.warn(`${cleanstamp} [WARN] [${this.proc}] - `, ...args);
		return this;
	}

	error (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.error(`${time} [${"ERROR".red}] [${this.proc}] - `, ...args);
		logFile.warn(`${cleanstamp} [ERROR] [${this.proc}] - `, ...args);
		return this;
	}

	crit (...args) {
		var time = getTimestamp();
		var cleanstamp = getCleanTimestamp();
		console.error(`${time} [${"CRITICAL".white.bgRed}] [${this.proc}] - `, ...args, "\n\tTensa can no longer continue and shall now shut down.");
		logFile.warn(`${cleanstamp} [CRITICAL] [${this.proc}] - `, ...args);
		process.exit(-1);
		return this;
	}
}

module.exports = logger;
