module.exports = {

	// Sharding Manager
	Shard: require("./sharding/Shard"),
	ShardingManager: require("./sharding/ShardingManager"),

	// Logger
	logger: require("./logger"),

	// Bot.
	Command: require("./baseCommand"),
	Watcher: require("./Watcher"),

	// Utilities
	Util: {
		fileExists: require("./fileExists"),
		readDir: require("./readDir"),
		Constants: require("./constants"),
		addToGlobal: require("./globalDefines"),
	},
};
