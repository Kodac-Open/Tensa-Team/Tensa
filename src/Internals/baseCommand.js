module.exports = class Command {
	constructor (client) {
		this.client = client;
		this.log = this.client.log;

		// It may not look like it but there is a zero-width space after the "\n".
		this.newLine = "\n​";

		// It may not look like it but there is a zero-width space before the "\n".
		this.newLineBefore = "​\n";

		// Extras.
		this.gitUrl = require("../../package.json").repository.url.substring(0, require("../../package.json").repository.url.indexOf(".git"));
		this.tVer = require("../../package.json").version;
	}

	/**
	 * Utility function to determine if a command can run
	 * @returns {Boolean}
	 */
	requirements () {
		return true;
	}

	/**
	 * The main command function.
	 */
	async run () {
		throw new Error(`The ${this.constructor.name} command doesn't have a run function!`);
	}

	async _run (params = {}) {
		try {
			if (this.requirements(params)) this.run(params);
			else this.requirementsFail(params);
		} catch (err) {
			this.log.error(`Error executing ${this.constructor.name}\n ${err}`)
			config.botMaintainers.forEach(m => {
				client.users.get(m).send({
					embed: {
						color: this.client.Constants.embedColours.ERROR,
						title: `An error has occoured:`,
						description: err,
						thumbnail: {
							url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
						},
					},
				});
			});
		}
	}

	// eslint-disable-next-line no-empty-function
	requirementsFail () {

	}

	notMaintainer ({ msg }) {
		msg.channel.send({
			embed: {
				color: this.client.Constants.embedColours.UNAUTHORISED,
				description: `You do not have permision to run this command!`,
				thumbnail: {
					url: "https://cdn.discordapp.com/attachments/408878735136391168/409489776740270089/lock.png",
				},
			},
		});
	}
};
