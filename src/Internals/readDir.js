const fs = require("fs");
const path = require("path");

// Make Promise version of fs.readdir()
// eslint-disable-next-line no-undef
module.exports = dir => new Promise((resolve, reject) => {
	fs.readdir(path.resolve(__dirname, "..", dir), (err, filenames) => {
		if (err) {
			reject(err);
		} else {
			resolve(filenames);
		}
	});
});
