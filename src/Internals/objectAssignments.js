// Object defines.
Object.defineProperty(Array.prototype, "chunk", {
  value: function value(n) {
    return Array.from(Array(Math.ceil(this.length / n)), (_, i) => this.slice(i * n, (i * n) + n));
  },
});

// Object assignments.
Object.assign(String.prototype, {
  escapeRegex() {
    const matchOperators = /[|\\{}()[\]^$+*?.]/g;
    return this.replace(matchOperators, "\\$&");
  },
});