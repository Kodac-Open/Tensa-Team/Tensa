const fs = require("fs");
const path = require("path");

// Make a Promise version of fs.exists()
// eslint-disable-next-line no-undef
module.exports = file => new Promise((resolve, reject) => {
	fs.existsSync(path.resolve(__dirname, "..", file), (exists, err) => {
		if (err) {
			reject(err);
		} else {
			resolve(exists);
		}
	});
});
