const { ShardingManager: Manager, Util } = require("discord.js");
const { Error, TypeError, RangeError } = require("discord.js/src/errors");
const Shard = require("./Shard");

class ShardingManager extends Manager {
	constructor (...args) {
		super(...args);
		this.Shard = Shard;
	}

	createShard (id = this.shards.size) {
		const shard = new this.Shard(this, id, this.shardArgs);
		this.shards.set(id, shard);
		this.emit("shardCreate", shard);
		return shard;
	}

	async spawn (amount = this.totalShards, delay = 5500, waitForReady = true) {
		// Obtain/verify the number of shards to spawn
		if (amount === "auto") {
			amount = await Util.fetchRecommendedShards(this.token);
		} else {
			if (typeof amount !== "number" || isNaN(amount)) {
				throw new TypeError("CLIENT_INVALID_OPTION", "Amount of shards", "a number.");
			}
			if (amount < 1) throw new RangeError("CLIENT_INVALID_OPTION", "Amount of shards", "at least 1.");
			if (amount !== Math.floor(amount)) {
				throw new TypeError("CLIENT_INVALID_OPTION", "Amount of shards", "an integer.");
			}
		}

		// Make sure this many shards haven't already been spawned
		if (this.shards.size >= amount) throw new Error("SHARDING_ALREADY_SPAWNED", this.shards.size);
		this.totalShards = amount;

		// Spawn the shards
		for (let s = 1; s <= amount; s++) {
			const promises = [];
			const shard = this.createShard();
			promises.push(shard.spawn(waitForReady));
			if (delay > 0 && s !== amount) promises.push(Util.delayFor(delay));
			await Promise.all(promises); // eslint-disable-line no-await-in-loop
		}

		return this.shards;
	}
}

module.exports = ShardingManager;
