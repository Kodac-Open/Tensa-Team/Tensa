let constant = exports;

constant.embedColours = {

	// Reusable embed colours.

	// Reds.
	CRIT_ERR: 0x8B0000,

	ERROR: 0xCB5259,

	RED: 0xff0000,

	// Blues.
	INFO: 0x1E90FF,

	BLUE: 0x0000FF,

	// Yellows.
	UNAUTHORISED: 0xF1A44B,

	WARN: 0xffcc4d,

	YELLOW: 0xffff00,

	// Greens.
	SUCCESS: 0x39BC82,

	GREEN: 0x00FF2B,

	// Oranges.
	LOADING: 0xFFC251,

	ORANGE: 0xFFA500,

	// White.
	WHITE: 0xFFFFFF,

	// Command specific colours.

	// Uber command.
	UBER: 0x1fbad6,

	UBER_NONE: 0x222233,
	
};

constant.images = {
	WARNING: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",

	CROSS: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",

	CLOWN: "https://cdn.discordapp.com/attachments/408878735136391168/437073410997223434/sssssss.png",

	GEAR: "https://cdn.discordapp.com/attachments/408878735136391168/409971473894080513/gear.png",

	MONEY: "https://cdn.discordapp.com/attachments/408878735136391168/414723479351328768/money.png",

	DOG: "https://cdn.discordapp.com/attachments/408878735136391168/410337670120865793/dog.png",

	CAT: "https://cdn.discordapp.com/attachments/408878735136391168/410355296989478912/cat.png",
}

constant.embeds = {
	argsErr: {
		color: constant.embedColours.WARN,
		description: `This command requires you to **supply arguments**!`,
		thumbnail: {
			url: "https://cdn.discordapp.com/attachments/408878735136391168/408889014020472833/warning.png",
		},
	},

	unkErr: {
		color: constant.embedColours.ERROR,
		description: `Oh no, an unknown error has occoured!`,
		thumbnail: {
			url: "https://cdn.discordapp.com/attachments/408878735136391168/408878856846573568/cross.png",
		},
	}
};
