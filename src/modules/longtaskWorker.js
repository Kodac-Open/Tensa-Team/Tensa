const { Client } = require("discord.js");
const logger = require("../Internals/logger");
const log = new logger(`Long Task Workers`);

if (config.shardAmount !== "none") {
	const driver = require("../database/");
	const db = new driver(config.dbUrl, log);
	db.connect().then(worker);
} else {
	worker();
}

function worker (db = global.db) {
	const client = new Client();
	client.on("ready", () => {
		client.guilds.forEach(guild => {
			db.fetchOrCreateGuild(guild).then(g => {
				if (g.new) log.info(`Added new guild ${guild.name} (${g.Guild.id}) to the database!`);
			});
		});
	});

	client.login(config.botToken).then(() => {
		log.suc("Succesfuly started Database worker");
	}).catch(err => {
		log.error(`An error occoured while logging into Discord \n${err}`);
	});
}
